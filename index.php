<!--
    File: index.php
    Purpose: output the results in a table
    Author: Mohamad Arafat
-->

<?php
include "lab2db.php";
?>

<html>
    <head>
        <title></title>
    </head>
    <body>

            <table>
                <tr>
                    <th>Radius</th>
                    <th>Volume</th>
                    <th>Surface Area</th>
                </tr>
                <?php
                    $allRecords= recSelection();

                    foreach ($allRecords as $row) {
                        echo "<tr>";
                        foreach ($row as $column) {
                            echo "<td>" . $column . "</td>";
                        }
                        echo "</tr>";
                    }
                    
                ?>
            </table>
    </body>
</html>