<?php
    declare(strict_types=1);
?>
<!--
    File: lab2.php
    Purpose: do the Mathmetical functions and the foreach loops
    Author: Mohamad Arafat
-->

<?php
	include 'lab2db.php';

	function randomSpheres (int $num) : array
    {
        
        $spheres = array();


        for ($i=0; $i < $num; $i++) { 

            $radii = rand(1, 20);

            array_push($spheres, $radii);
        }

        return $spheres;
    }

	function surArea(int $radii) : float {
        $sur_area = 4 * pi() * pow($radii, 2);

        return $sur_area;
    }

	function volSphere(int $radii) : float {
	$vol_sphere = 4/3 * pi() * pow($radii, 3) ;

	return $vol_sphere;
	}

    $spheres = randomSpheres(20);
    
	foreach ($spheres as $radius) {
        $surfaceA = surArea($radius);
        $volumeSph = volSphere($radius);
        
	    recordSph($radius, $volumeSph, $surfaceA);
    }

?>
