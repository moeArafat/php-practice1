<!--
    File: lab2db.php
    Purpose: PDO connection to lab2.php's database
    Author: Mohamad Arafat
-->

<?php

	define("DBHOST", "localhost");
	define("DBDB", "lab2");
	define("DBUSER", "lamp1user");
	define("DBPW", "!Lamp12!");

	function connectDB(){
		$dsn = "mysql:host=".DBHOST.";dbname=".DBDB.";charset=utf8";
		try{
			$db_conn = new PDO($dsn, DBUSER, DBPW);
			return $db_conn;
		} catch (PDOException $e){
			echo "<p>Error opening database <br/>\n".$e->getMessage()."</p>\n";
			exit(1);
		}
	}

	function recordSph($radius, $volume, $surface_area) {
        $dbc = connectDB();

        	$stmt = $dbc->prepare('INSERT INTO spheres (radius, volume, surface_area) values(:radius, :volume, :surface_area)');
        
		if (!$stmt) {
		    echo "Error ".$dbc->errorCode()."\nMessage ".implode($dbc->errorInfo())."\n";
		    exit(1);
		}
		$data = array(":radius" => $radius, ":volume" => $volume, ":surface_area" => $surface_area);
		$status = $stmt->execute($data);
		
		if(!$status) {
		    echo "Error ".$stmt->errorCode()."\nMessage ".implode($stmt->errorInfo())."\n";
		    exit(1);
		}
	$dbc = null;
	}
	
	function recSelection() {
        $dbc = connectDB();
        $result = array();

        $stmt = $dbc->prepare("SELECT radius, volume, surface_area FROM spheres;");
        if (!$stmt) {
            echo "Error ".$dbc->errorCode()."\nMessage ".implode($dbc->errorInfo())."\n";
            exit(1);
        }

        $status = $stmt->execute();

        if ($status) {
            if ($stmt->rowCount() > 0) {
                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    $tmp_dimensions = [];
                    foreach ($row as $key => $value) {
                        array_push($tmp_dimensions, $value);                
                    }

                    if (count($tmp_dimensions) > 0) {                        
                        array_push($result, $tmp_dimensions);
                    }
                }
            }
        }
        else {
            echo "Error ".$stmt->errorCode()."\nMessage ".implode($stmt->errorInfo())."\n";
            exit(1);
        }

        return $result;
  }

?>
